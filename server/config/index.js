// import .env variables
require('dotenv').config()

module.exports = {
  node_env: process.env.NODE_ENV,
  host: process.env.HOST,
  port: process.env.PORT,
  database: {
    main: {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      name: process.env.DB_NAME,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      dialect: 'mysql',
      autoreconnect: true,
      logging: false
    },
    log: {
      host: process.env.LOG_DB_HOST,
      port: process.env.LOG_DB_PORT,
      name: process.env.LOG_DB_NAME,
      username: process.env.LOG_DB_USERNAME,
      password: process.env.LOG_DB_PASSWORD,
      dialect: 'mysql',
      autoreconnect: true,
      logging: false
    }
  },
  password_salt: process.env.PASSWORD_SALT
}
