const router = require('express').Router()
const Device = require('../models').device
const checkData = require('../middleware/dataChecker')
const checkDevice = require('../middleware/deviceChecker')
const checkAuth = require('../middleware/authChecker')
const { requestLogger } = require('../middleware/logger')
const sendResponse = require('../helpers/responseSender')
const getUserOperations = require('../helpers/userOperationsGetter')
const {
  createUser,
  processLogout
} = require('../controller/user')
const {
  select: processMenuSelection
} = require('../controller/menuOperation')
const {
  select: select_part_selector,
  scan: scan_part_selector
} = require('../controller/partSelectorOperation')
const {
  select: select_part_info,
  scan: scan_part_info
} = require('../controller/partInfoOperation')

// merging is needed to access easily to operation controllers
const operations = {
  select_part_selector,
  scan_part_selector,
  select_part_info,
  scan_part_info
}

router.get('/', requestLogger, checkData, checkDevice, async (req, res) => {
  const device_id = req.query.id

  const device = await Device.findOne({
    where: {
      device_id
    }
  })

  if (device.page_location === 'menu') {
    return processMenuSelection(req, res)
  }
  else if (device.page_location && device.fk_user) {
    // get paired user operation list
    // if page location matches with any op list id
    // process operation selection
    const response = await getUserOperations(device.fk_user)
    if (response.status) {
      const userOperations = response.data.id_list
      if (userOperations.includes(device.page_location)) {
        try {
          return operations[`select_${device.page_location}`](req, res)
        } catch (err) {
          return sendResponse(res, 'error', {
            screen: {
              icon: 'err',
              text: 'Internal \nServer Error',
            },
            error: {
              code: 500,
              message: err.message
            }
          })
        }
      }
      // else page location not matched with user operations
      // then process logout
      return processLogout(req, res)
    }
    // if getUserOperations function return error
    return sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: response.error.code,
        message: response.error.message
      }
    })
  }
  return processLogout(req, res)
})

router.post('/', requestLogger, checkData, checkDevice, checkAuth, async (req, res) => {
  const {
    action,
    param,
    device_id
  } = req.body

  const device = await Device.findOne({
    where: {
      device_id
    }
  })

  if (action === 'select') {
    if (param === 'menu') {
      return processMenuSelection(req, res)
    }
    else if (param === 'logout') {
      return processLogout(req, res)
    }
    // get paired user operation list
    // if param matches with any op list id
    // process operation selection
    const response = await getUserOperations(device.fk_user)
    if (response.status) {
      const userOperations = response.data.id_list
      if (userOperations.includes(param)) {
        try {
          return operations[`${action}_${param}`](req, res)
        } catch (err) {
          return sendResponse(res, 'error', {
            screen: {
              icon: 'err',
              text: 'Internal \nServer Error',
            },
            error: {
              code: 500,
              message: err.message
            }
          })
        }
      }
      return sendResponse(res, 'error', {
        screen: {
          icon: 'error',
          text: 'Invalid Operation'
        },
        error: {
          code: 400,
          message: 'Unrecognized operation selection'
        }
      })
    }
    // if getUserOperations function return error
    return sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: response.error.code,
        message: response.error.message
      }
    })
  }
  else if (action === 'scan') {
    // get paired user operation list
    // if page location matches with any op list id
    // process operation scan
    const response = await getUserOperations(device.fk_user)
    if (response.status) {
      const userOperations = response.data.id_list
      if (userOperations.includes(device.page_location)) {
        try {
          return operations[`${action}_${device.page_location}`](req, res)
        } catch (err) {
          return sendResponse(res, 'error', {
            screen: {
              icon: 'err',
              text: 'Internal \nServer Error'
            },
            error: {
              code: 500,
              message: err.message
            }
          })
        }
      }
      // else device is lost, and server sends menu with no indicators
      // create menu list
      const menu = []
      response.data.full_list.forEach(el => {
        const item = {
          id: el.op_id,
          name: el.name
        }
        menu.push(item)
      })
      return sendResponse(res, 'none', {
        screen: {
          menu,
          top_info: device.fk_user,
          touch_button: {
            id: 'logout',
            name: 'Logout'
          }
        },
        locked: true,
        error: {
          code: 400,
          message: 'Unrecognized page location based on operation list in database. Server redirects to the menu'
        }
      })
    }
    // getUserOperations function error
    return sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: response.error.code,
        message: response.error.message
      }
    })
  }
  // return action value error
  sendResponse(res, 'error', {
    screen: {
      icon: 'error',
      text: 'Invalid \nAction Type'
    },
    error: {
      code: 400,
      message: 'The request body action value is not recognized'
    }
  })
})

// create user
router.post('/user', createUser)

module.exports = router
