const jwt = require('jsonwebtoken')
const { Op } = require('sequelize')
const User = require('../models').user
const Device = require('../models').device
const { password_salt } = require('../config')
const sendResponse = require('../helpers/responseSender')
const getUserOperations = require('../helpers/userOperationsGetter')

module.exports = {
  processLogin,
  processLogout,
  createUser
}


/**
  * @desc authenticate & pair device with user credentials
  * @param object req - http request
  * @param object res - http response
  * @returns void - function call
*/
async function processLogin (req, res) {
  const {
    device_id,
    param
  } = req.body
  try {
    // param format: username=joedoe&password=securepass
    const username = param.split('&')[0].split('=')[1]
    const password = param.split('&')[1].split('=')[1]
    const user = await User.findOne({
      where: {
        username,
        is_active: true
      }
    })
    if (!user) {
      return sendResponse(res, 'warning', {
        screen: {
          icon: 'warn',
          text: 'User is \nnot found'
        },
        error: {
          code: 404,
          message: `User is not found with "${username}" username`
        }
      })
    }
    // if user paired with another device
    const pairedDevice = await Device.findOne({
      where: {
        device_id: {
          [Op.ne]: device_id
        },
        fk_user: username
      }
    })
    // deauthorize device and unset paired user
    if (pairedDevice) {
      await Device.update({
        is_authenticated: false,
        page_location: null,
        fk_user: null
      }, {
        where: {
          device_id: pairedDevice.device_id
        }
      })
    }
    // verify password
    if (verifyPassword(user.password, password)) {
      await Device.update({
        is_authenticated: true,
        page_location: 'menu',
        fk_user: username,
        last_active_time: Date.now()
      }, {
        where: {
          device_id
        }
      })

      const response = await getUserOperations(username)
      if (!response.status) {
        // if getUserOperations function return error
        return sendResponse(res, 'error', {
          screen: {
            icon: 'err',
            text: 'Internal \nServer Error'
          },
          error: {
            code: response.error.code,
            message: response.error.message
          }
        })
      }
      // create menu list
      const menu = []
      response.data.full_list.forEach(el => {
        const item = {
          id: el.op_id,
          name: el.name
        }
        menu.push(item)
      })
      return sendResponse(res, 'info', {
        screen: {
          menu,
          top_info: username,
          touch_button: {
            id: 'logout',
            name: 'Logout'
          }
        },
        locked: true
      })
    }
    return sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Invalid user \ncredentials'
      },
      error: {
        code: 400,
        message: 'Invalid user credentials to authenticate'
      }
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

/**
  * @desc unauthorize & unpair device
  * @param object req - http request
  * @param object res - http response
  * @returns void - function call
*/
async function processLogout (req, res) {
  var device_id
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
  }
  try {
    await Device.update({
      is_authenticated: false,
      page_location: null,
      fk_user: null,
      last_active_time: Date.now()
    }, {
      where: {
        device_id
      }
    })
    return sendResponse(res, 'info', {
      screen: {
        icon: 'ok',
        text: 'Please \nLog In',
        top_info: 'Logged Out'
      }
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

/**
 * @desc creates a new user
 * @param req object - http request
 * @param res object - http response
 * @returns void - http response
*/
async function createUser (req, res) {
  const {
    username,
    firstname,
    lastname,
    password
  } = req.body

  if (
    (!username || typeof username !== 'string') ||
    (!firstname || typeof firstname !== 'string') ||
    (!lastname || typeof lastname !== 'string') ||
    (!password || typeof password !== 'string')
  ) {
    return res.status(401).json('Request body is not accurate.')
  }
  // check username in database
  try {
    const user = await User.findOne({
      where: {
        username
      }
    })
    if (user) {
      return res.status(401).json('Username is already exists.')
    }

    await User.create({
      username,
      firstname,
      lastname,
      password: encryptPassword(password)
    })
    return res.status(200).json(`User created with the ${username} username.`)
  } catch (err) {
    return res.status(500).json({
      error: {
        code: err.code,
        message: err.message
      }
    })
  }
}

/**
  * @desc generates encrypted password with HS256 algorithm
  * @param string rawPassword - user password value
  * @returns string - encrypted token
*/
function encryptPassword (rawPassword) {
  return jwt.sign({ rawPassword }, password_salt)
}
/**
  * @desc verifies raw password with encrypted
  * @param string encryptedPassword - encrypted user password
  * @param string rawPassword - raw user password
  * @returns boolean
*/
function verifyPassword (encryptedPassword, rawPassword) {
  const decodedPassword = jwt.verify(encryptedPassword, password_salt)
  if (decodedPassword.rawPassword === rawPassword) {
    return true
  }
  return false
}
