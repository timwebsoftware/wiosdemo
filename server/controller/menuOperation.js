const Device = require('../models').device
const sendResponse = require('../helpers/responseSender')
const getUserOperations = require('../helpers/userOperationsGetter')

/**
  * @desc updates device page location and binds menu items into response
  * @param req object - http request
  * @param res object - http response
  * @returns void - sendResponse function call
*/
async function select (req, res) {
  var device_id
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
  }
  const device = await Device.findOne({
    where: {
      device_id
    }
  })
  const response = await getUserOperations(device.fk_user)
  if (!response.status) {
    // if getUserOperations function return error
    return sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: response.error.code,
        message: response.error.message
      }
    })
  }
  try {
    // update device page location
    await Device.update({
      page_location: 'menu',
      last_active_time: Date.now()
    }, {
      where: {
        device_id
      }
    })
    // create menu list
    const menu = []
    response.data.full_list.forEach(el => {
      const item = {
        id: el.op_id,
        name: el.name
      }
      menu.push(item)
    })
    sendResponse(res, 'info', {
      screen: {
        menu,
        top_info: device.fk_user,
        touch_button: {
          id: 'logout',
          name: 'Logout'
        }
      },
      locked: true
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

module.exports = { select }
