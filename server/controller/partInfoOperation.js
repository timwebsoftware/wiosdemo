const Device = require('../models').device
const Product = require('../models').product
const OperationLog = require('../models').operation_log
const sendResponse = require('../helpers/responseSender')
/**
  * @desc updates device page location
  * @param req object - http request
  * @param res object - http response
  * @returns void - sendResponse function call
*/
async function select (req, res) {
  var device_id
  var param
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
    param = req.body.param
  }

  try {
    if (req.method === 'GET') {
      const device = await Device.findOne({
        where: { device_id }
      })
      param = device.page_location
    }
    // update device page location
    await Device.update({
      page_location: param,
      last_active_time: Date.now()
    }, {
      where: {
        device_id
      }
    })
    sendResponse(res, 'info', {
      screen: {
        text: 'Scan Product \nBarcode',
        top_info: 'Part Info',
        touch_button: {
          id: 'menu',
          name: 'Menu'
        }
      }
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

/**
  * @desc updates device page location and creates operation log
  * @param req object - http request
  * @param res object - http response
  * @returns void - sendResponse function call
*/
async function scan (req, res) {
  const {
    param,
    device_id
  } = req.body
  try {
    const device = await Device.findOne({
      where: {
        device_id
      }
    })
    const product = await Product.findOne({
      where: {
        barcode: param
      }
    })
    // if product not found
    if (!product) {
      return sendResponse(res, 'error', {
        screen: {
          text: 'Unknown \nProduct',
          top_info: 'Part Info',
          touch_button: {
            id: 'menu',
            name: 'Menu'
          }
        }
      })
    }

    await OperationLog.create({
      fk_operation: device.page_location,
      fk_user: device.fk_user,
      fk_device: device.device_id,
      fk_product: product.barcode,
      done_at: Date.now()
    })

    if (product.quantity > 0) {
      return sendResponse(res, 'success', {
        screen: {
          icon: 'ok',
          text: `${product.name} \nQty: ${product.quantity}`,
          top_info: 'Part Info',
          touch_button: {
            id: 'menu',
            name: 'Menu'
          }
        }
      })
    }
    return sendResponse(res, 'warning', {
      screen: {
        icon: 'warn',
        text: `${product.name} \nOut of Stock`,
        top_info: 'Part Info',
        touch_button: {
          id: 'menu',
          name: 'Menu'
        }
      }
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

module.exports = {
  select,
  scan
}
