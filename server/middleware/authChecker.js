const Device = require('../models').device
const User = require('../models').user
const sendResponse = require('../helpers/responseSender')
const { processLogin } = require('../controller/user')
/**
  * @desc checks auth state of the device
  * @param object req - http request
  * @param object res - http response
  * @param function next - callback function
  * @returns null
*/
module.exports = async function checkAuth (req, res, next) {
  const {
    action,
    param,
    device_id
  } = req.body
  const paramFormatForLogin = /^username=[\S\s]+&password=[\S\s]+/
  try {
    // if incoming is login request
    if (action === 'scan' && paramFormatForLogin.test(param)) {
      return processLogin(req, res)
    }
    // get device
    const device = await Device.findOne({
      where: {
        device_id
      }
    })
    // device is authenticated
    if (device.is_authenticated) {
      // check timeout (6 hours)
      const currentTime = Date.now()
      const lastActivity = new Date(device.last_active_time || Date.now())
      if (diffHours(currentTime, lastActivity) > 6) {
        await Device.update({
          is_authenticated: false,
          page_location: null,
          fk_user: null,
          last_active_time: Date.now()
        }, {
          where: {
            device_id: device.device_id
          }
        })
        return sendResponse(res, 'warning', {
          screen: {
            icon: 'warn',
            text: 'Please \nLog In'
          },
          error: {
            code: 401,
            message: 'The request requires authentication'
          }
        })
      }
      const user = await User.findOne({
        where: {
          username: device.fk_user
        }
      })
      if (!user) {
        await Device.update({
          is_authenticated: false,
          page_location: null,
          fk_user: null,
          last_active_time: Date.now()
        }, {
          where: {
            device_id
          }
        })
        return sendResponse(res, 'warning', {
          screen: {
            icon: 'warn',
            text: 'User is not recognized, login again'
          },
          error: {
            code: 401,
            message: 'Paired user is not found, login with another user'
          }
        })
      }
      // update last active time
      await Device.update({
        last_active_time: Date.now()
      }, {
        where: {
          device_id
        }
      })
      // call next process
      return next()
    }
    sendResponse(res, 'warning', {
      screen: {
        icon: 'warn',
        text: 'Please \nLog In'
      },
      error: {
        code: 401,
        message: 'The request requires authentication'
      }
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

function diffHours (dt1, dt2) {
  var diff = (dt1.getTime() - dt2.getTime()) / 1000
  diff /= (60 * 60)
  return Math.abs(Math.round(diff))
}
