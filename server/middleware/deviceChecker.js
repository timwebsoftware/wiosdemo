const Device = require('../models').device
const deviceErrorLogger = require('../middleware/deviceErrorLogger')
const sendResponse = require('../helpers/responseSender')
/**
  * @desc checks device with device_id in db
  * @param object req - http request
  * @param object res - http response
  * @param function next - callback function
  * @returns void
*/
module.exports = async function checkDevice (req, res, next) {
  var device_id
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
  }
  try {
    // query device in db with device_id
    const device = await Device.findOne({
      where: {
        device_id
      }
    })
    if (!device) {
      return sendResponse(res, 'warning', {
        screen: {
          icon: 'warn',
          text: 'Device is not \nrecognized'
        },
        error: {
          code: 404,
          message: `Device with "${device_id}" ID  is not found`
        }
      })
    }
    // log the internal device error
    if (req.method === 'POST') {
      await deviceErrorLogger(device_id, req.body.error)
    }
    // call next process
    return next()
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}
