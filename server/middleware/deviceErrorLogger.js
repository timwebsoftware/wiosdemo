const uuid = require('uuid/v1')
const { device_errors: DeviceError } = require('../models').log.models
const { node_env } = require('../config')
/**
 * @desc logs internal device errors
 * @param string device_id - device identifier
 * @param object error - device errors
 * @returns null
*/
module.exports = async function deviceErrorLogger (device_id, error) {
  if (node_env !== 'production') {
    return
  }
  if (
    !error ||
    (!error.code && !error.message)
  ) {
    return
  }
  try {
    const error_code = error.code
    const error_message = error.message
    const error_time = Date.now()

    await DeviceError.create({
      uuid: uuid(),
      device_id,
      error_code,
      error_message,
      error_time
    })
    return
  } catch (err) {
    console.log(err)
  }
}
