const sendResponse = require('../helpers/responseSender')
/**
  * @desc validates http request body
  * @param object req - http request
  * @param object res - http response
  * @param function next - callback function
  * @returns void
*/
module.exports = function checkData (req, res, next) {
  if (req.method === 'GET') {
    const device_id = req.query.id
    // check param
    if (typeof device_id === 'string' && device_id.length > 0) {
      // call next process
      return next()
    }
    return sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Request \nerror'
      },
      error: {
        code: 400,
        message: 'Request param is not valid'
      }
    })
  }
  else if (req.method === 'POST') {
    const action = req.body.action
    const param = req.body.param
    const device_id = req.body.device ? req.body.device.id : null
    const battery_level = req.body.device ? req.body.device.battery_level : null
    const battery_health = req.body.device ? req.body.device.battery_health : null
    if (
      (typeof action === 'string' && action.length > 0) &&
      (typeof param === 'string' && param.length > 0) &&
      (typeof device_id === 'string' && device_id.length > 0) &&
      Number.isInteger(battery_level) &&
      Number.isInteger(battery_health)
    ) {
      // reassign request body with the deconstructed data
      req.body = {
        ...req.body,
        action,
        param,
        device_id,
        battery_health,
        battery_level
      }
      // call next process
      return next()
    }
    return sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Invalid \nRequest Body'
      },
      error: {
        code: 400,
        message: 'Request body data is not accurate'
      }
    })
  }
}
