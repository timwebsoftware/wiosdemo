const uuid = require('uuid/v1')
const { logs: Log } = require('../models').log.models
const { node_env } = require('../config')
/**
  * @desc logs http request
  * @param object req - http request
  * @param object res - http response
  * @param function next - callback function
  * @returns void
*/
async function requestLogger (req, res, next) {
  if (node_env !== 'production') {
    return next()
  }
  try {
    const request_method = req.method
    const request_content_type = req.get('content-type')
    const request_user_agent = req.get('user-agent')
    const request_content_length = req.get('content-length')
    const request_body = request_method === 'POST' ? JSON.stringify(req.body) : JSON.stringify(req.query)
    const request_time = Date.now()
    await Log.create({
      uuid: uuid(),
      request_method,
      request_content_type,
      request_user_agent,
      request_content_length,
      request_body,
      request_time
    })
    return next()
  } catch (err) {
    console.log(err)
    next()
  }
}

/**
  * @desc logs http response
  * @param object res - http response
  * @param object resBody - response body
*/
async function responseLogger (res, resBody) {
  if (node_env !== 'production') {
    return
  }
  try {
    const response_status_code = res.statusCode
    const response_status_message = res.statusMessage
    const response_content_length = res.get('content-length')
    const response_body = JSON.stringify(resBody)
    const response_time = Date.now()

    const latestLog = await Log.findOne({
      order: [[ 'request_time', 'DESC' ]]
    })

    await Log.update({
      response_status_code,
      response_status_message,
      response_content_length,
      response_body,
      response_time
    }, {
      where: {
        uuid: latestLog.uuid
      }
    })
    return
  } catch (err) {
    console.log(err)
  }
}

module.exports = {
  requestLogger,
  responseLogger
}
