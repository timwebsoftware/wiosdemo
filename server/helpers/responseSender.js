const { node_env } = require('../config')
const { responseLogger } = require('../middleware/logger')
/**
  * @desc prepares response json data and returns http response
  * @param object res - http response object
  * @param string type - type of the response: success, info, warning, error
  * @param object data {screen, locked, error} - config data
  * @returns void - http response
*/
module.exports = async function sendResponse (res, type, data = {}) {
  const screen = data.screen || {}
  const locked = typeof data.locked === 'boolean' ? data.locked : false
  const error = data.error || {}
  var config = {}
  try {
    config = {
      indicator: {},
      screen: {
        menu: screen.menu || [],
        icon: screen.icon || 'none',
        text: screen.text || '',
        top_info: screen.top_info || '',
        touch_button: screen.touch_button || {},
        sleep_clear: screen.sleep_clear || 'off',
        sleep_time: screen.sleep_time || 10000
      },
      locked,
      error: {
        code: error.code || null,
        message: error.message || null
      }
    }

    switch (type) {
      case 'success':
      case 'info':
      case 'warning':
      case 'error':
      case 'none':
        config.indicator = indicators[type]
        break
      default:
        config.indicator = indicators.none
        break
    }
  } catch (err) {
    if (node_env === 'development') {
      console.log(err)
    }
    process.exit(1)
  }
  // send http response
  res.status(200).json(config)

  // log response
  await responseLogger(res, config)
}

var indicators = {
  success: {
    light: {
      left: 'green',
      right: 'green',
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    buzzer: {
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    vibration: {
      control: 'off',
      on_time: 100,
      off_time: 100,
      repeat: 1
    }
  },
  info: {
    light: {
      left: 'blue',
      right: 'blue',
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    buzzer: {
      control: 'off',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    vibration: {
      control: 'off',
      on_time: 100,
      off_time: 100,
      repeat: 1
    }
  },
  warning: {
    light: {
      left: 'yellow',
      right: 'yellow',
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    buzzer: {
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    vibration: {
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 1
    }
  },
  error: {
    light: {
      left: 'red',
      right: 'red',
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 2
    },
    buzzer: {
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 2
    },
    vibration: {
      control: 'on',
      on_time: 100,
      off_time: 100,
      repeat: 2
    }
  },
  none: {
    light: {
      left: 'red',
      right: 'red',
      control: 'off',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    buzzer: {
      control: 'off',
      on_time: 100,
      off_time: 100,
      repeat: 1
    },
    vibration: {
      control: 'off',
      on_time: 100,
      off_time: 100,
      repeat: 1
    }
  }
}
