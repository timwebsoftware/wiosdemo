const { Op } = require('sequelize')
const User = require('../models').user
const Operation = require('../models').operation
const UserOperation = require('../models').user_operation
/**
 * @desc paired user operations list getter
 * @param string username - username value
 * @returns array - operation list
*/
module.exports = async function getUserOperations (username) {
  try {
    const user = await User.findOne({
      where: {
        username
      },
      include: [{
        model: UserOperation,
        as: 'operations'
      }]
    })
    if (
      typeof user === 'object' &&
      Array.isArray(user.operations) &&
      user.operations.length > 0
    ) {
      const opIds = user.operations.map((opId) => opId.fk_operation)
      const ops = await Operation.findAll({
        where: {
          op_id: {
            [Op.or] : opIds
          }
        }
      })
      if (Array.isArray(ops) && ops.length > 0) {
        return {
          status: true,
          data: {
            id_list:  opIds,
            full_list: ops
          }
        }
      }
      return {
        status: false,
        error: {
          code: 404,
          message: 'Operations not found in the DB'
        }
      }
    }
    return {
      status: false,
      error: {
        code: 404,
        message: 'No accessible operations for this user'
      }
    }
  } catch (err) {
    return {
      status: false,
      error: {
        code: err.code || 500,
        message: err.message
      }
    }
  }
}
