module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('product', {
    barcode: {
      field: 'barcode',
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      allowNull: false
    },
    name: {
      field: 'name',
      type: DataTypes.STRING,
      allowNull: false
    },
    quantity: {
      field: 'quantity',
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    underscored: true
  })

  Product.associate = (models) => {
    Product.hasMany(models.operation_log, {
      foreignKey: 'fk_product'
    })
  }

  Product.prototype.toPublic = function () {
    return Object.assign({}, this.get())
  }

  return Product
}
