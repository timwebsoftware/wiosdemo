module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    username: {
      field: 'username',
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      allowNull: false
    },
    firstname: {
      field: 'firstname',
      type: DataTypes.STRING,
      allowNull: false
    },
    lastname: {
      field: 'lastname',
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      field: 'password',
      type: DataTypes.STRING,
      allowNull: false
    },
    is_active: {
      field: 'is_active',
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    underscored: true
  })

  User.associate = (models) => {
    User.hasMany(models.user_operation, {
      foreignKey: 'fk_user',
      as: 'operations'
    })
    User.hasOne(models.device, {
      foreignKey: 'fk_user'
    })
  }

  User.prototype.toPublic = function () {
    let values = Object.assign({}, this.get())

    delete values.password
    return values
  }

  return User
}
