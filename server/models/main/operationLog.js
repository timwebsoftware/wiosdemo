module.exports = (sequelize, DataTypes) => {
  const OperationLog = sequelize.define('operation_log', {
    done_at: {
      field: 'done_at',
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    }
  }, {
    underscored: true
  })

  OperationLog.associate = (models) => {
    OperationLog.belongsTo(models.operation, {
      foreignKey: 'fk_operation',
      as: 'operation'
    })
    OperationLog.belongsTo(models.user, {
      foreignKey: 'fk_user',
      as: 'user'
    })
    OperationLog.belongsTo(models.device, {
      foreignKey: 'fk_device',
      as: 'device'
    })
    OperationLog.belongsTo(models.product, {
      foreignKey: 'fk_product',
      as: 'product'
    })
  }

  OperationLog.prototype.toPublic = function () {
    return Object.assign({}, this.get())
  }

  return OperationLog
}
