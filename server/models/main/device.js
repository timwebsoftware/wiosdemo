module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define('device', {
    device_id: {
      field: 'device_id',
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      allowNull: false
    },
    model: {
      field: 'model',
      type: DataTypes.STRING,
      allowNull: false
    },
    details: {
      field: 'details',
      type: DataTypes.STRING,
      allowNull: true
    },
    page_location: {
      field: 'page_location',
      type: DataTypes.STRING,
      default: null
    },
    is_authenticated: {
      field: 'is_authenticated',
      type: DataTypes.BOOLEAN,
      default: false
    },
    last_active_time: {
      field: 'last_active_time',
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    }
  }, {
    underscored: true
  })

  Device.associate = (models) => {
    Device.belongsTo(models.user, {
      foreignKey: 'fk_user',
      as: 'user'
    })
  }

  return Device
}
