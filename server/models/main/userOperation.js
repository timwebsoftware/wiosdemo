module.exports = (sequelize) => {
  const UserOperation = sequelize.define('user_operation', {}, {
    underscored: true
  })

  UserOperation.associate = (models) => {
    UserOperation.belongsTo(models.user, {
      foreignKey: 'fk_user',
      as: 'user'
    })
    UserOperation.belongsTo(models.operation, {
      foreignKey: 'fk_operation',
      as: 'operation'
    })
  }

  return UserOperation
}
