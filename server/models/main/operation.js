module.exports = (sequelize, DataTypes) => {
  const Operation = sequelize.define('operation', {
    op_id: {
      field: 'op_id',
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      allowNull: false
    },
    name: {
      field: 'name',
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    }
  }, {
    underscored: true
  })

  Operation.associate = (models) => {
    Operation.hasMany(models.user_operation, {
      foreignKey: 'fk_operation',
      as: 'operations'
    })
  }

  Operation.prototype.toPublic = function () {
    return Object.assign({}, this.get())
  }

  return Operation
}
