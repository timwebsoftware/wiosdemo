const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const basename = path.basename(module.filename)
const config = require('../config')
const db = {}
const databases = Object.keys(config.database)

// add databases
for (let i = 0; i < databases.length; i++) {
  let database = databases[i]
  let dbConfigs = config.database[database]
  db[database] = new Sequelize(dbConfigs.name, dbConfigs.username, dbConfigs.password, {
    dialect: dbConfigs.dialect,
    host: dbConfigs.host,
    port: dbConfigs.port,
    autoreconnect: dbConfigs.autoreconnect,
    logging: dbConfigs.logging
  })
}

// add models of the main db
fs
  .readdirSync(__dirname + '/main')
  .filter((file) =>
    (file.indexOf('.') !== 0) &&
    (file !== basename) &&
    (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = db.main.import(path.join(__dirname + '/main', file))
    db[model.name] = model
  })

// add models of the log db
fs
  .readdirSync(__dirname + '/log')
  .filter((file) =>
    (file.indexOf('.') !== 0) &&
    (file !== basename) &&
    (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = db.log.import(path.join(__dirname + '/log', file))
    db[model.name] = model
  })

for (let key in db) {
  if (db[key].associate) {
    db[key].associate(db)
  }
}

db.main.authenticate()
  .then(() => {
    console.log('Main DB connected')
  })
  .catch((err) => {
    console.log(new Error(err))
    process.exit(1)
  })
// sync method is only for development purposes
db.main.sync({ alter: false, force: false })
  .then(() => {
    console.log('Main DB synced')
  })
  .catch((err) => {
    console.log(new Error(err))
    process.exit(1)
  })

db.log.authenticate()
  .then(() => {
    console.log('Log DB connected')
  })
  .catch((err) => {
    console.log(new Error(err))
    process.exit(1)
  })
// sync method is only for development purposes
db.log.sync({ alter: false, force: false })
  .then(() => {
    console.log('Log DB synced')
  })
  .catch((err) => {
    console.log(new Error(err))
    process.exit(1)
  })

module.exports = db
