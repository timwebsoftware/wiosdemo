module.exports = (sequelize, DataTypes) => {
  const DeviceError = sequelize.define('device_errors', {
    uuid: {
      field: 'uuid',
      type: DataTypes.UUID,
      primaryKey: true
    },
    device_id: {
      field: 'device_id',
      type: DataTypes.STRING,
      defaultValue: null
    },
    error_code: {
      field: 'error_code',
      type: DataTypes.STRING,
      defaultValue: null
    },
    error_message: {
      field: 'error_message',
      type: DataTypes.STRING,
      defaultValue: null
    },
    error_time: {
      field: 'error_time',
      type: DataTypes.DATE,
      defaultValue: null
    }
  }, {
    underscored: true
  })

  return DeviceError
}
