module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define('logs', {
    uuid: {
      field: 'uuid',
      type: DataTypes.UUID,
      primaryKey: true
    },
    request_method: {
      field: 'request_method',
      type: DataTypes.STRING,
      defaultValue: null
    },
    request_content_type: {
      field: 'request_content_type',
      type: DataTypes.STRING,
      defaultValue: null
    },
    request_user_agent: {
      field: 'request_user_agent',
      type: DataTypes.STRING,
      defaultValue: null
    },
    request_content_length: {
      field: 'request_content_length',
      type: DataTypes.STRING,
      defaultValue: null
    },
    request_body: {
      field: 'request_body',
      type: DataTypes.TEXT,
      defaultValue: null
    },
    request_time: {
      field: 'request_time',
      type: DataTypes.DATE,
      defaultValue: null
    },
    response_status_code: {
      field: 'response_status_code',
      type: DataTypes.STRING,
      defaultValue: null
    },
    response_status_message: {
      field: 'response_status_message',
      type: DataTypes.STRING,
      defaultValue: null
    },
    response_content_length: {
      field: 'response_content_length',
      type: DataTypes.STRING,
      defaultValue: null
    },
    response_body: {
      field: 'response_body',
      type: DataTypes.TEXT,
      defaultValue: null
    },
    response_time: {
      field: 'response_time',
      type: DataTypes.DATE,
      defaultValue: null
    }
  }, {
    underscored: true
  })

  return Log
}
