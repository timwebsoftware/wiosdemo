INSERT INTO `devices` (`device_id`, `model`, `details`, `page_location`, `is_authenticated`, `last_active_time`, `created_at`, `updated_at`, `fk_user`) VALUES
('102', 'WIOS 5 GHz ER', NULL, NULL, 0, '2019-08-26 22:49:52', '2019-08-06 00:00:00', '2019-08-26 22:49:52', NULL);

INSERT INTO `operations` (`op_id`, `name`, `created_at`, `updated_at`) VALUES
('part_info', 'Part Info', '2019-08-06 00:00:00', '2019-08-06 00:00:00'),
('part_selector', 'Part Selector', '2019-08-06 00:00:00', '2019-08-06 00:00:00');

INSERT INTO `products` (`barcode`, `name`, `quantity`, `created_at`, `updated_at`) VALUES
('1921ZY', 'Cylinder Block', 2, '2019-07-02 00:00:00', '2019-07-02 00:00:00'),
('22XG4V', 'Cylinder', 4, '2019-07-04 00:00:00', '2019-07-04 00:00:00'),
('22XGSG', 'Piston', 0, '2019-07-04 00:00:00', '2019-07-04 00:00:00'),
('22XGUH', 'Combustion chamber', 43, '2019-07-02 00:00:00', '2019-07-02 00:00:00'),
('22XHAT', 'Exhaust manifold', 32, '2019-07-02 00:00:00', '2019-07-02 00:00:00'),
('23JBX7', 'Spark plug', 11, '2019-07-04 00:00:00', '2019-07-04 00:00:00'),
('23JDLE', 'Connecting Rod', 0, '2019-07-04 00:00:00', '2019-07-04 00:00:00'),
('23JGIO', 'Crankshaft', 23, '2019-07-02 00:00:00', '2019-07-02 00:00:00'),
('2694428', 'Piston rings', 0, '2019-07-04 00:00:00', '2019-07-04 00:00:00'),
('4292441', 'Camshaft', 40, '2019-07-04 00:00:00', '2019-07-04 00:00:00');


INSERT INTO `users` (`username`, `firstname`, `lastname`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
('user1', 'First', 'User', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyYXdQYXNzd29yZCI6InBhc3N3b3JkMSIsImlhdCI6MTU2Njg4ODE2Nn0.74As1xB8TNnxjaBT5A2qx_S1qmw3WmjfjjbLlVROtQc', 1, '2019-08-19 08:16:01', '2019-08-19 08:16:01'),
('user2', 'Second', 'User', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyYXdQYXNzd29yZCI6InBhc3N3b3JkMiIsImlhdCI6MTU2Njg4ODE4NX0.t8fA9cJnT9Dv1iYaMTwC0hjCY6jRCBnpxsHHbGwB8vg', 1, '2019-08-22 08:38:07', '2019-08-22 08:38:07'),
('user3', 'Third', 'User', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyYXdQYXNzd29yZCI6InBhc3N3b3JkMyIsImlhdCI6MTU2Njg4ODIwMH0.Otk5Twac18SYJAW-As0ZmeH_HhLQm_Aug0qF3ifnT2Q', 1, '2019-08-06 16:35:27', '2019-08-06 16:35:27'),
('user4', 'Fourth', 'User', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyYXdQYXNzd29yZCI6InBhc3N3b3JkNCIsImlhdCI6MTU2Njg4ODIxMX0.PYRw66xqrNkrKh-uHtWWyeK0Yxr2FzMZIIkdJl1n8a4', 1, '2019-08-21 09:24:16', '2019-08-21 09:24:16');

INSERT INTO `user_operations` (`id`, `created_at`, `updated_at`, `fk_operation`, `fk_user`) VALUES
(1, '2019-08-06 00:00:00', '2019-08-06 00:00:00', 'part_info', 'user1'),
(2, '2019-08-07 00:00:00', '2019-08-07 00:00:00', 'part_selector', 'user1'),
(3, '2019-08-19 00:00:00', '2019-08-19 00:00:00', 'part_info', 'user2'),
(4, '2019-08-19 00:00:00', '2019-08-19 00:00:00', 'part_selector', 'user2'),
(5, '2019-08-21 00:00:00', '2019-08-21 00:00:00', 'part_info', 'user3'),
(6, '2019-08-21 00:00:00', '2019-08-21 00:00:00', 'part_selector', 'user3'),
(7, '2019-08-22 00:00:00', '2019-08-22 00:00:00', 'part_info', 'user4'),
(8, '2019-08-22 00:00:00', '2019-08-22 00:00:00', 'part_selector', 'user4');
