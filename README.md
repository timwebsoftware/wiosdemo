# TIM Demo Server - Setup Guide

> Before starting the setup, make sure you have installed docker and docker-compose.
> Follow the steps to complete this demo setup successfully.

## Requirements

First you need [Docker Compose](https://docs.docker.com/compose/install/) to run the server on your local machine. Docker Compose relies to Docker Engine, so make sure you have Docker Engine and Docker Compose installed either.

> We assume that you have the WIOS device and server API software provided by us.

```sh
~$ docker-compose --version
docker-compose version 1.24.0, build 0aa59064
```

## 1. Move into Server directory

Download the project files into your machine

```sh
cd ~/[parent_dir]/demo-server/
```

## 2. Run the docker-compose command  

Open up your terminal and move into the downloaded repo folder.
Then run the command below;

```sh
docker-compose up -d --build
```

## 3. Import the sample data

Initial data is defined in the `data/sample-data.sql` file.
Run the following command in your terminal

> Before importing sample data, make sure you have installed [MySQL Connector/NET](https://dev.mysql.com/downloads/connector/net/) in your Windows Machine

```sh
mysql -h 127.0.0.1 -udbuser -pdbpassword api-server < ./data/sample-data.sql
```

## 4. Check the imported data

After importing the demo data into the database, it will be good to check the imported data.
For checking database follow the commands step by step.

### Connect to DB

```sh
mysql -h 127.0.0.1 -udbuser -pdbpassword api-server
```

### Query the tables

```sql
show tables;
select * from devices;
select * from users;
select * from operations;
select * from user_operations;
```

## Summary

It's that easy to run the demo server in your machine.  
You can fully manage and manipulate database and server either.

**Note:** After you make changes to the project source files, make sure to run the following command to apply changes to docker containers.
`docker-compose up -d --build --force-recreate`

